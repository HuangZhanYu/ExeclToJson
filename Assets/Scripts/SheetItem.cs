﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using NPOI.SS.UserModel;
using System.IO;
public class SheetItem : MonoBehaviour {
    public UILabel sheetName;
    public UIToggle ObjType;
    public UIToggle arrType;
    public UIInput fileNameInput;
    public GameObject outPutBtn;
    public Action<ISheet,string,int> parseEvent;
    public ISheet sheet;
	// Use this for initialization
	void Start () 
    {
        fileNameInput.defaultText = "导出的json文件名";
        addEvent();
	}

    private void addEvent()
    {
        UIEventListener.Get(outPutBtn).onClick = onClickOutPutBtn;
    }

    public void updateSheet(ISheet sheet)
    {
        this.sheet = sheet;
        sheetName.text = sheet.SheetName;

    }

    private void onClickOutPutBtn(GameObject go)
    {
        int parseType = 0;
        if (ObjType.value == true)
        {
            parseType = 1;
        }
        else if (arrType.value == true)
        {
            parseType = 2;
        }
        if (parseType == 0)
        {
            return;
        }
        if (string.IsNullOrEmpty(fileNameInput.value))
        {
            return;
        }
        string dirPath = OpenFileWindow.openFileDialog();
        if (string.IsNullOrEmpty(dirPath))
        {
            return;
        }
        string jsonPath = Path.Combine(dirPath,fileNameInput.value);
        if (parseEvent != null)
        {
            parseEvent(sheet, jsonPath, parseType);
        }
    }
	
}
