﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.HSSF.UserModel;
using System.IO;
using System.Text;
using UnityEngine;

public class ExcelTools
{

    public IWorkbook updateWorkBook(string excelFilePath)
    {
        IWorkbook workBook = null;
        ICSharpCode.SharpZipLib.Zip.ZipConstants.DefaultCodePage = Encoding.UTF8.CodePage;
        using (FileStream fileStream = File.Open(excelFilePath, FileMode.Open, FileAccess.Read,FileShare.ReadWrite))
        {
            if (excelFilePath.IndexOf(".xlsx") > 0) // 07以上版本
            {
                workBook = new XSSFWorkbook(fileStream);
            }
            else if (excelFilePath.IndexOf(".xls") > 0) //03版本
            {
                workBook = new HSSFWorkbook(fileStream);
            }
        }
        return workBook;
    }

    /// <summary>
    /// 解析成json对象
    /// </summary>
    /// <param name="sheet"></param>
    public string parseToJsonObj(ISheet sheet,string jsonPath)
    {
        if (sheet == null)
        {
            return "{}";
        }
        string str = "{\n\t";
        int rowCount = sheet.LastRowNum; //最后
        // 0 行描述  1行字段类型 2行字段名 3行开始就是值
        IRow rowValueType = sheet.GetRow(1);
        IRow rowKey = sheet.GetRow(2);
        for (int i = 3; i <= rowCount; i++) //行
        {
            IRow row = sheet.GetRow(i);
            ICell idCell =  row.GetCell(0);
            if (idCell == null)
            {
                Debug.LogError(">>>>>>>>>>>id一定不能为空行数为："+i);
                continue;
            }
            string id = idCell.ToString();
            if (string.IsNullOrEmpty(id))
            {
                continue;
            }
            string result = "";
            if (i!=3)
            {
                result = ",\n\t" + "\"" + id + "\"" + ":{";
            }
            else
            {
                result = addStringTips(id) + ":{";
            }
            for (int j = 1; j < row.LastCellNum; j++) // 列
            {
                string valueType = null;//值类型
                string key = null; //字段名
                string value = null;//
                ICell cell = null;
                cell = rowValueType.GetCell(j);
                if (cell != null)
                {
                    valueType = cell.ToString();
                }
                cell =  rowKey.GetCell(j);
                if (cell != null)
	            {
                    key = cell.ToString();
	            }
                cell = row.GetCell(j);
                if (cell != null)
                {
                    value = row.GetCell(j).ToString();
                }
                key = addStringTips(key);
                if (string.IsNullOrEmpty(valueType)|| string.IsNullOrEmpty(key)) //如果中间出现中断的情况 后续的格子都不会再去遍历了
                {
                    break;
                }
                value = parseValue(valueType,value);
                if (j !=1)
                {
                    result += string.Format(",\n\t\t{0}:{1}", key, value);
                }
                else
                {
                    result += string.Format("\n\t\t{0}:{1}", key, value);
                }
            }
            result += "\n\t}";
            str += result;
        }
        str += "\n}";
        writeText(jsonPath, str);
        return str;
    }
    /// <summary>
    /// 解析成json 数组
    /// </summary>
    /// <param name="sheet"></param>
    /// <returns></returns>
    public string parseToJsonArray(ISheet sheet,string jsonPath)
    {
        if (sheet == null)
        {
            return "{}";
        }
        string str = "{\n\t";
        int rowCount = sheet.LastRowNum; //最后

        // 0 行描述  1行字段类型 2行字段名 3行开始就是值
        IRow rowValueType = sheet.GetRow(1);
        IRow rowKey = sheet.GetRow(2);
        string result = "";
        for (int i = 3; i <= rowCount; i++) //行
        {
            IRow row = sheet.GetRow(i);
            ICell idCell = row.GetCell(0);
            string id = null;
            if (idCell != null)
            {
                id = idCell.ToString();
            }
            string content = "";
            if (string.IsNullOrEmpty(id)) //id 是空的  这行数值还是属于上一个的
            {
                content+= ",\n\t\t{";
            }
            else //该id段结束
            {
                content+= "\n\t\t{";
                if (i != 3)
                {
                    result += "\n\t]";
                    result += ",\n\t" + "\"" + id + "\"" + ":[";
                }
                else
                {
                    result += addStringTips(id) + ":[";
                }
            }
            for (int j = 1; j < row.LastCellNum; j++) // 列
            {
                string valueType = null;//值类型
                string key = null; //字段名
                string value = null;//
                ICell cell = null;
                cell = rowValueType.GetCell(j);
                if (cell != null)
                {
                    valueType = cell.ToString();
                }
                cell = rowKey.GetCell(j);
                if (cell != null)
                {
                    key = cell.ToString();
                }
                cell = row.GetCell(j);
                if (cell != null)
                {
                    value = row.GetCell(j).ToString();
                }
                key = addStringTips(key);
                if (string.IsNullOrEmpty(valueType) || string.IsNullOrEmpty(key)) //如果中间出现中断的情况 后续的格子都不会再去遍历了
                {
                    break;
                }
                value = parseValue(valueType, value);
                if (j != 1)
                {
                    content += string.Format(",\n\t\t\t{0}:{1}", key, value);
                }
                else
                {
                    content += string.Format("\n\t\t\t{0}:{1}", key, value);
                }
            }
            content += "\n\t\t}";
            result += content;
            if (i == rowCount)
            {
                result += "\n\t]";
            }
        }
        str += result;
        str += "\n}";
        writeText(jsonPath,str);
        return str;
    }
    private void writeText(string jsonPath,string str)
    {
        File.WriteAllText(jsonPath, str);
    }
    /// <summary>
    /// 添加字符串符号
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    private string addStringTips(string value)
    {
        value = "\""+value+"\"";
        return value;
    }
    /// <summary>
    /// 表格内容解析成字符串
    /// </summary>
    /// <param name="valueType"> 值类型</param>
    /// <param name="value"> 值</param>
    /// <returns>返回解析好的值</returns>
    private string parseValue(string valueType, string value)
    {
        string result = "";
        if (valueType == "string")
        {
            if (string.IsNullOrEmpty(value))
            {
                result = "\"\"";
            }
            else
            {
                result = "\""+value+"\"";
            }
        }
        else if (valueType == "boolean")
        {
            if (string.IsNullOrEmpty(value))
            {
                result = "false";
            }
            else
            {
                bool tem;
                if (Boolean.TryParse(value,out tem))
                {
                    result = tem.ToString().ToLower();
                }
                else
                {
                    result = "false";
                }
            }
        }
        else if (valueType == "number")
        {
            if (string.IsNullOrEmpty(value))
            {
                result = "0";
            }
            else
            {
                float tem;
                if (float.TryParse(value, out tem))
                {
                    result = tem.ToString();
                }
                else
                {
                    result = "0";
                }
            }
        }
        else if (valueType == "array")
        {
            if (string.IsNullOrEmpty(value))
            {
                result = "[]";
            }
            else
            {
                result = "[ " + value + " ]";
            }
        }
        else
        {
            Debug.LogError(">>>>>>>>>>>不支持的类型:"+valueType);
        }
        return result;
    }
}
