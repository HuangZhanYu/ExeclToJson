﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NPOI.SS.UserModel;
public class ParseExcelPanel : MonoBehaviour {

    public GameObject parseBtn;
    public UIGrid grid;
    public GameObject workBookItem;
    private IWorkbook workBook;
    private ExcelTools excelTools;
    private List<SheetItem> sheetItemList;
    void Awake()
    {
        excelTools = new ExcelTools();
        sheetItemList = new List<SheetItem>();
    }
	// Use this for initialization
	void Start () 
    {
        UIEventListener.Get(parseBtn).onClick = onClickParseBtn;
	}

    private void onClickParseBtn(GameObject go)
    {
        string excelPath = OpenFileWindow.GetOpenFileExcel();
        if (string.IsNullOrEmpty(excelPath))
        {
            return;
        }
        Debug.Log(excelPath);
        workBook = excelTools.updateWorkBook(excelPath);
        if (workBook != null)
        {
            updateSheetItemCount(workBook.NumberOfSheets);
            setSheetItemData();
        }
    }

    private void updateSheetItemCount(int sheetCount)
    {
        int count = sheetCount - sheetItemList.Count;
        for (int i = 0; i < count; i++)
        {
            GameObject item = Instantiate(workBookItem) as GameObject;
            SheetItem sheetItem = item.GetComponent<SheetItem>();
            item.transform.SetParent(grid.transform,false);
            sheetItemList.Add(sheetItem);
            sheetItem.parseEvent = outputJson;
            sheetItem.ObjType.group = i + 1;
            sheetItem.arrType.group = i + 1;
        }
        if (count < 0)
        {
            for (int i = sheetCount; i < sheetItemList.Count; i++)
            {
                SheetItem sheetItem = sheetItemList[i];
                sheetItem.gameObject.SetActive(false);
            }
        }
        grid.repositionNow = true;
    }
	
    private void setSheetItemData()
    {
        for (int i = 0; i < workBook.NumberOfSheets; i++)
        {
            SheetItem sheetItem = sheetItemList[i];
            ISheet sheet = workBook.GetSheetAt(i);
            sheetItem.updateSheet(sheet);
            sheetItem.gameObject.SetActive(true);
        }
    }

    private void outputJson(ISheet sheet,string jsonPath, int parseType)
    {
        if (string.IsNullOrEmpty(jsonPath))
        {
            return;
        }
        if (jsonPath.IndexOf(".json")< 0)
        {
            jsonPath = jsonPath + ".json";
        }
        if (parseType ==1)
        {
            excelTools.parseToJsonObj(sheet, jsonPath);
        }
        else if (parseType == 2)
        {
            excelTools.parseToJsonArray(sheet, jsonPath);
        }
        string dir = System.IO.Path.GetDirectoryName(jsonPath);
        //System.Diagnostics.Process.Start(dir);
    }
}
